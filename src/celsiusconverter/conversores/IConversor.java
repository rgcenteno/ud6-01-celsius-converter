/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package celsiusconverter.conversores;

/**
 *
 * @author Rafael González Centeno
 */
public interface IConversor {
    public int fromAtoB(int a);
    public int fromBtoA(int b);
    public String getStringA();
    public String getStringB();
}
